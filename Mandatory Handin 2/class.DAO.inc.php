<?php
class DAO{

	private $dbusername = "root";
	private $dbpassword = "root";
	private $dsn = "mysql:host=localhost;dbname=mandatory2";

	private $db;
	

	//public functions
	public function testIfUserExists($email){
		$this->connect();
		$statement = $this->db->prepare("SELECT email FROM users WHERE email='$email'");
		$statement->execute();
		if ($statement->rowCount() > 0) {
			$this->disconnect();
			return true;
		}
		
		
	}
	
	public function createNewUser($email, $password){
		
		$hashedpassword = $this->cryptpassword($password, $email);
		
		$this->connect();
		try {
			$this->db->beginTransaction();
			$statement = $this->db->prepare("INSERT INTO users (email, password)" . "VALUES (?,?)");
			$statement->execute(array("$email", "$hashedpassword"));
			$this->db->commit();	
		}
		catch (Exception $error) {
			$db->rollback();
			echo "Transaction not completed: " . $error->getMessage();
		}
		$this->disconnect();
	}
	
	
	
	
	public function login($email, $password){
		
		$hashedpassword = $this->cryptpassword($password, $email);
		
		$this->connect();
		$statement = $this->db->prepare("SELECT email FROM users WHERE email='$email' AND password='$hashedpassword'");
		$statement->execute();
		if ($statement->rowCount() > 0) {
			$this->disconnect();
			return true;
		}
		else{
			$this->disconnect();
			return false;
		}
		
	}
	
	
	//crypt password
	private function cryptpassword($password, $email){
		
		$salt = $email;
		$hashedpassword = crypt($password, $salt);
		
		
		return $hashedpassword;
	}
	
	
	//connect and diconnection function
	private function connect(){
		$this->db = new PDO ($this->dsn, $this->dbusername, $this->dbpassword);
	}
	
	private function disconnect(){
		$this->db = NULL;
	}
}

?>