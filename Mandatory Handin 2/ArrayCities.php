<?php 
$pageTitle = "MH2 | ArrayCities";
include 'pagetop.php';?>


<article>

	<h1>ArrayCities</h1>
	<?php 
	$array = array(
	"Tokyo", "Mexico City", "New York City", "Mumbai", "Seoul", "Shanghai", "Lagos", "Buenos Aires", 
			"Cairo", "London");
	?>
	<h2>Unsorted array</h2>	
	
			<?php 
			echo "<h3>Iterate over the array with loop:</h3> <p>";
			
			$size = count($array) - 1;
			foreach ($array as $key => $value) {
				if ($size == $key){
					echo "{$value}.";
				}
				else {
					echo "{$value}, ";
				}
			}
			echo "</p>";
			
			echo "<h3>... and the neat way to do it with implode:</h3> <p>";
			
			$comma_separated = implode(", ", $array);
			echo "{$comma_separated}.</p>";
			
			?>
	<h2>Sorted array</h2>
		<?php 
		echo "<h3>Iterate over the array with loop:</h3> <p>";
		sort($array);
		$size = count($array) - 1;
		foreach ($array as $key => $value) {
			if ($size == $key){
				echo "{$value}.";
			}
			else {
				echo "{$value}, ";
			}
		}
		echo "</p>";
		
		echo "<h3>... and the neat way to do it with implode:</h3> <p>";
		
		$comma_separated = implode(", ", $array);
		echo "{$comma_separated}.</p>";
		
		?>
	<h2>Add cities and write as UL</h2>
		
		<?php 
		array_push($array, "Los Angeles", "Calcutta", "Osaka", "Beijing");
		sort($array);
		?>
		<ul>
			<?php 
			foreach ($array as $value) {
				echo "<li>$value</li>";
			}?>
		
		
		</ul>
</article>


<?php include 'pagebottom.php';?>