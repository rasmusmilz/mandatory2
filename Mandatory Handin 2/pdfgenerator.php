<?php
require 'fpdf17/fpdf.php';



if (isset($_POST['buy'])){
	$adress = $_POST['address'];
	$adressnr = $_POST['housenumber'];
	$postnr = $_POST['zip'];
	$city = $_POST['city'];
	$date = date("m.d.y");
	$item1 = $_POST['soda'];
	$item1Amount = $_POST['sodaamount'];
	$item2 = $_POST['snack'];
	$item2Amount = $_POST['snackamount'];
	


}



$pdf = new FPDF();

$pdf->addPage();
$pdf->setFont("Arial", "", 32);
$pdf->SetX(40);
$pdf->Cell(130, 15, "Mandatory2 WebShop", 1, 1, "C");

$pdf->ln();
$pdf->ln();
$pdf->setFont("Arial", "", 13);
$pdf->Cell(0, 3, "$adress $adressnr", "", 1, "L");
$pdf->ln();
$pdf->Cell(150, 3, "{$postnr}, $city", "", 0, "L");

$pdf->Cell(0, 3, "Date: $date", "", 0, "L");
$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->ln();$pdf->ln();$pdf->ln();$pdf->ln();$pdf->ln();$pdf->ln();
$pdf->setFont("Arial", "", 17);
$pdf->Cell(0, 5, "Invoice", "", 1, "L");
$pdf->ln();
$pdf->Line(10, 95, 200, 95);
$pdf->setFont("Arial", "", 11);

for ($i = 0; $i < $item1Amount; $i++){
	$pdf->Cell(170, 5, $item1, "", 0, "L");
	$pdf->Cell(0, 5, "20.00", "", 1, "L");
}
for ($i = 0; $i < $item2Amount; $i++){
	$pdf->Cell(170, 5, $item2, "", 0, "L");
	$pdf->Cell(0, 5, "6.00", "", 1, "L");
}

$pdf->Line(10, 150, 200, 150);

$pdf->SetY(155);
//calculate items dkk
$totalValue = ($item1Amount * 20) + ($item2Amount*6);

$pdf->Cell(170, 5, "Items DKK", "", 0, "L");
$pdf->Cell(0, 5, "{$totalValue}.00", "", 1, "L");

//calculate taxes
$taxes = ($totalValue/100) *25;

$pdf->Cell(170, 5, "Taxes 25%", "", 0, "L");
$pdf->Cell(0, 5, "$taxes", "", 1, "L");

//calculate total value
$total = $taxes + $totalValue;

$pdf->Cell(170, 5, "TOTAL DKK", "B", 0, "L");
$pdf->Cell(0, 5, "$total", "B", 1, "L");

$pdf->ln();
$pdf->ln();
$pdf->Cell(0, 5, "BANK: Danske Bank", "", 1, "L");
$pdf->Cell(0, 5, "ACCOUNT: 1234 1234567890", "", 1, "L");
$pdf->Cell(0, 5, "PAYMENT: net 8 days. Interest accrued at 2.0 % per month thereafter", "", 1, "L");



$pdf->setFont("Arial", "B", 9);
$pdf->SetXY(160, 250);
$pdf->Cell(0, 5, "Mandatory2", "", 1, "L");
$pdf->setX(160);
$pdf->setFont("Arial", "", 9);
$pdf->Cell(0, 5, "Rasmus J�rgensen", "", 1, "L");
$pdf->setX(160);
$pdf->Cell(0, 5, "Akacievej 33", "", 1, "L");
$pdf->setX(160);
$pdf->Cell(0, 5, "2791 Drag�r", "", 1, "L");
$pdf->setX(160);
$pdf->Cell(0, 5, "51888543", "", 1, "L");

$pdf->output();