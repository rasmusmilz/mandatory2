<?php 
$pageTitle = "MH2 | Rasmus";
include 'pagetop.php';?>


	<article>
			
				<h1>Mandatory Handin 2</h1>
				<p>
					This handin is with the same layout as my Mandatory handin 1, and focus has been on the log-in
					system.
				</p>
				<h2>ArrayCities</h2>
				
				<p>
					Sorting the cities as written in the assignment, and printing them out for the user.			
				</p>
				
				<h2>Checkbox Array</h2>
 				<p>
 					Checkbox array is a non-sticky form, but calls the same document with a post and displays 
 					the result below the checkbox.
 				</p>	

					
				<h2>Log-in</h2>
 				<p>
 					The log-in is build on the index.php file, so will be the first thing the user sees. 
 					If the user dont have an account already, there is a button next to the login button. 
 					this redirects the user to a createUser page. In the create user-page the user can create
 					a new account - here it makes sure that the user inputs long enough password(not numbers 
 					and letters) and an email for username. 
 					After creation the user can log-in and browse the page(and are unable to login / create new user
 					for as long as still logged in).
 				</p>
 				

			</article>
					



<?php include 'pagebottom.php';?>