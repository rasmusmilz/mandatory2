<?php session_start();

if(isset($_GET['logout'])){
	session_destroy();
	header('Location: index.php');
}

if(!isset($_SESSION['email'])){
	header('Location: index.php?missinglogin=true');
}

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="reset.css" />
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<title>
			<?php echo $pageTitle; ?> 
		</title>
</head>

<body>
	<header>
		<a href="index.php"> <img alt="header-image" src="php-banner.jpg">
		</a>

		<h1>
			Mandatory <br> Handin 2
		</h1>
		<p>
			By Rasmus J&oslash;rgensen<br>
				Logged in as: <?php echo $_SESSION['email'];?></p>
		<nav>
			<ul>
				<li><a href="frontpage.php?logout=true">logout</a></li>
			</ul>
		</nav>
	</header>

	<nav>
		<ul>
			<li><a href="frontpage.php">Frontpage</a></li>
			<li><a href="ArrayCities.php">ArrayCities</a></li>
			<li><a href="checkboxarray.php">CheckboxArray</a></li>
			<li><a href="webshop.php">Web Shop</a></li>
		</ul>
	</nav>
	<main>