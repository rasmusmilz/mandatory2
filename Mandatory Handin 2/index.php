<?php
session_start();

//if user already logged in:
if(isset($_SESSION['email'])){
	header('Location: frontpage.php');
}


//redirect to newuser page if button is clicked
if (isset($_POST['newuser'])){
	header('Location: newuser.php');
}


//if login button is clicked
if (isset($_POST['login'])){
	include_once 'class.DAO.inc.php';
	$DAO = new DAO();
	$result = $DAO->login($_POST['email'], $_POST['password']);
	if($result){
		$_SESSION['email'] = $_POST['email'];
		header('Location: frontpage.php');
	}
	else{
		$errormessage = "Login data didnt match!";
	}
	
}

//if new user is created set the message
if(isset($_GET['newuser'])){
	$errormessage = "New user created succesfully!";
}

//if user tries to view the site without login
if(isset($_GET['missinglogin'])){
	$errormessage = "you must login before viewing the site";
}





?>


<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<title>MH2 | login</title>

</head>

<body>
	<header>
		<a href="index.php">
			<img alt="header-image" src="php-banner.jpg">
		</a>
			
			<h1>
				Mandatory <br> Handin 2
			</h1>
			<p>By Rasmus J&oslash;rgensen</p>

	</header>





	<!-- The input form -->
	<form id="loginform" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
		<fieldset>
			<legend>Login</legend>
			<?php if(isset($errormessage)){ echo '<p class="errormessage">'."$errormessage".' <p><br>';}?>
			<label for="email">Username(email):</label> 
			<input type="email" name="email" autofocus/>
			<label for="password">Password:</label> 
			<input type="password" name="password"  /> 
			<input class="button" type="submit" value="Login" name="login"/>
			<input class="button" type="submit" value="New user" name="newuser"/>
		</fieldset>
	</form>
	
		
	
</body>
</html>