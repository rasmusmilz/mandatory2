<?php 
session_start();
//if user already logged in:
if(isset($_SESSION['email'])){
	header('Location: frontpage.php');
}

$dbusername = "root";
$dbpassword = "root";
$dsn = "mysql:host=localhost;dbname=mandatory2";

if (isset($_POST['newuser'])) {
	
	$email = $_POST['email'];
	$password1 = $_POST['password1'];
	$password2 = $_POST['password2'];

	//test the input!!!
	
	
	if (filter_var($email, FILTER_VALIDATE_EMAIL) && strlen($email) > 45){
		//not an email
		$errormessage = "Not a valid email";
	}
	elseif (strlen($password1) < 5){
		//password too short
		$errormessage = "Password must be atleast 5 digits";
	}
	elseif ($password1 != $password2){
		//passwords dont match
		$errormessage = "Passwords don't match";
	}else{
		
		include_once 'class.DAO.inc.php';
		$DAO = new DAO();
		
		if ($DAO->testIfUserExists($email)) {
			$errormessage = "Username already exists";
		}
		
	}
	
	//if no errormessage then create the user!
	if(!isset($errormessage)){
	
		//Doing the database work!
		$DAO->createNewUser($email, $password1);
		header('Location: index.php?newuser="true"');
	}
	
}




?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<title>MH2 | New user</title>

</head>

<body>
	<header>
		<a href="index.php">
			<img alt="header-image" src="php-banner.jpg">
		</a>
			
			<h1>
				Mandatory <br> Handin 2
			</h1>
			<p>By Rasmus J&oslash;rgensen</p>
	
	</header>





	<!-- The input form -->
	<form id="loginform" action="<?php echo $_SERVER['PHP_SELF']; ?>"
		method="POST">
		<fieldset>
			
			<legend>Create new user</legend>
			<?php
			if (isset($errormessage)){ echo '<p class="errormessage">'."$errormessage".' <p><br>';}
			?>
			<label for="email">Email(username):</label> 
			<input type="email" name="email" value="
			<?php if(isset($_POST['email'])){ echo $_POST['email'];}?>" required="required" autofocus />
			<label for="password1">Password:</label> 
			<input type="password" name="password1" required="required" /> 
			<label for="password2">Retype password:</label> 
			<input type="password" name="password2" required="required" />
			<label for="submit"></label> 
			<input type="submit" value="Submit" name="newuser"/>
		</fieldset>
	</form>
	
		
	
</body>
</html>