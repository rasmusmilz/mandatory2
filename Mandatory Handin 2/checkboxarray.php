<?php 
$pageTitle = "MH2 | CheckboxArray";
include 'pagetop.php';?>


<article>

	<h1>CheckBox as Array</h1>
	
	<?php 
	
	$buildings = array(
		"Red Building", "Tree House", "Fraternity Crib", "Utility Baracks", "Oak Complex");
	?>
	<h2>Mark the buildings you want to visit</h2>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
	
	<?php 

	foreach ($buildings as $key => $value) {
		//<input type="checkbox" name="myform[]" value="some_value1">Displayed value1<br/>
		echo '<input type="checkbox" name="'."$key".'" value="'."$value".'">'."$value".'<br/>';
	}
	
	?>
	
	<input type="submit" name="submit"/>
	</form>
	<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST') { 
		echo "<h3>Your choosen buildings:</h3><p>";
		$price = 0;
		foreach ($buildings as $key => $value) {
			if (isset($_POST[$key])){
				$price += 10;
				echo "$_POST[$key] </br>";
			}
		}
		if ($price == 0){
			echo "</br>No buildings choosen!</p>";
		}
		else{
			echo "</br>That will be: $price $</p>";	
		}
		
	}
				
	?>
		
</article>


<?php include 'pagebottom.php';?>